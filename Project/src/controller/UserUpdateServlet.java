package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		String targetId = request.getParameter("id");

		// 確認用：idをコンソールに出力
		//System.out.println(targetId);

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User user = userDao.UserDetail(targetId);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user);

		// ユーザー更新のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/** ユーザー更新 **/
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//URLからGETパラメータとしてIDを受け取る
		String targetId = request.getParameter("id");

		//データの取得
		UserDao userDao = new UserDao();
		User user = userDao.UserDetail(targetId);
		request.setAttribute("user", user);

		// リクエストパラメータの入力項目を取得;
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		/** (失敗1)パスワードとパスワード(確認)の入力内容が異なる場合 **/
		if (!(password.equals(confirmPassword))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** (失敗2)入力項目に1つでも未入力のものがある場合 **/
		if (name.equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 成功 **/
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDao.UserUpdate(password, name, birthDate, targetId);

		//一覧画面にリダイレクト
		response.sendRedirect("UserListServlet");
	}

}
