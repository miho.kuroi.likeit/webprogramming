package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDeleteServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		String targetId = request.getParameter("id");

		// 確認用：idをコンソールに出力
		//System.out.println(targetId);

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User user = userDao.UserDetail(targetId);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user);

		// ユーザー削除のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/delete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/** ユーザー削除 **/
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//URLからGETパラメータとしてIDを受け取る
		String targetId = request.getParameter("id");

		//データの取得
		UserDao userDao = new UserDao();
		User user = userDao.UserDetail(targetId);
		request.setAttribute("user", user);

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDao.UserDelete(targetId);

		//一覧画面にリダイレクト
		response.sendRedirect("UserListServlet");

	}

}
