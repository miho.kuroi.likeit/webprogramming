package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCreateServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*管理者と一般ユーザーで利用制限を設ける場合
		//セッションを開始
		HttpSession session = request.getSession(true);

		// ログイン情報を取得.
		User user = (User) session.getAttribute("userInfo");

		//ログインIDはadminか
		if (user.getLoginId().equals("admin")) {
			// adminは新規登録のjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request, response);
			return;
		} else {
			// その他は一覧画面にリダイレクト
			response.sendRedirect("UserListServlet");
			return;
		}
		*/

		// 新規登録のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/** 新規登録 **/
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		UserDao userDao = new UserDao();

		/** (失敗1)既に登録されているログインIDが入力された場合 **/
		if (userDao.IdCheck(loginId) == false) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** (失敗2)パスワードとパスワード(確認)の入力内容が異なる場合 **/
		if (!(password.equals(confirmPassword))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** (失敗3)入力項目に1つでも未入力のものがある場合 **/
		if (loginId.equals("") || password.equals("") || confirmPassword.equals("") || name.equals("")
				|| birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 成功 **/
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDao.UserCreate(loginId, password, name, birthDate);

		//一覧画面にリダイレクト
		response.sendRedirect("UserListServlet");

	}

}
