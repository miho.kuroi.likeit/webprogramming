package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	/**
	* ログインIDとパスワードでユーザーを探す
	*/
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文の準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, cryptWithMD5(password));
			ResultSet rs = pStmt.executeQuery();
			System.out.println(sql);

			// ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	* 全てのユーザ情報を取得する
	*/
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備（管理者以外）
			String sql = "SELECT * FROM user WHERE NOT id = 1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Timestamp sqlCreateDate = rs.getTimestamp("create_date");
				Timestamp sqlUpdateDate = rs.getTimestamp("update_date");

				//更新日と変更日を変換
				String createDate = new SimpleDateFormat("yyyy年MM月dd日 hh:mm").format(sqlCreateDate);
				String updateDate = new SimpleDateFormat("yyyy年MM月dd日 hh:mm").format(sqlUpdateDate);

				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}

			stmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	* 検索したユーザ情報を取得
	*/
	public List<User> findUser(String loginId, String name, String birthDateFrom, String birthDateTo) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備（管理者以外）
			String sql = "SELECT * FROM user WHERE id != '1'";

			if (!loginId.equals("")) {
				sql += " AND" + " login_id = '" + loginId + "'";
			}

			if (!name.equals("")) {
				sql += " AND" + " name LIKE '%" + name + "%'";
			}

			if (!birthDateFrom.equals("") && !birthDateTo.equals("")) {
				sql += " AND" + " birth_date BETWEEN '" + birthDateFrom + "' AND '" + birthDateTo + "'";
			}

			// SELECTを実行し、結果表を取得
			//System.out.println(sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				loginId = rs.getString("login_id");
				name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Timestamp sqlCreateDate = rs.getTimestamp("create_date");
				Timestamp sqlUpdateDate = rs.getTimestamp("update_date");

				//TODO:DATE型で取ってフォーマットを変えてString型に変換
				String createDate = new SimpleDateFormat("yyyy年MM月dd日 hh:mm").format(sqlCreateDate);
				String updateDate = new SimpleDateFormat("yyyy年MM月dd日 hh:mm").format(sqlUpdateDate);

				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}

			stmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 ** ユーザーを新規登録
	 **/
	public void UserCreate(String loginId, String password, String name, String birthDate) {
		Connection conn = null;
		PreparedStatement pStmt = null;

		//String型をDate型に変換
		Date sqlDate = Date.valueOf(birthDate);

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 実行SQL文字列定義
			String sql = "INSERT INTO user(login_id, name, birth_date, password, create_date, update_date) VALUES (?, ?, ?, ?, NOW(), NOW())";

			// ステートメント生成
			pStmt = conn.prepareStatement(sql);

			// SQLの?パラメータに値を設定
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setDate(3, sqlDate);
			pStmt.setString(4, cryptWithMD5(password));

			// 登録SQL実行
			pStmt.executeUpdate();

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	* ユーザ情報の詳細を取得
	*/
	public User UserDetail(String targetId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			//System.out.println(sql);
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// レコードの内容をUserインスタンスに設定
			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			Timestamp sqlCreateDate = rs.getTimestamp("create_date");
			Timestamp sqlUpdateDate = rs.getTimestamp("update_date");

			//TODO:DATE型で取ってフォーマットを変えてString型に変換
			String createDate = new SimpleDateFormat("yyyy年MM月dd日 hh:mm").format(sqlCreateDate);
			String updateDate = new SimpleDateFormat("yyyy年MM月dd日 hh:mm").format(sqlUpdateDate);

			return new User(id, loginId, name, birthDate, password, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 ** ユーザー情報を更新
	 **/
	public void UserUpdate(String password, String name, String birthDate, String targetId) {
		Connection conn = null;
		PreparedStatement pStmt = null;

		//String型をDate型に変換
		Date sqlBirthDate = Date.valueOf(birthDate);

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 実行SQL文字列定義
			String sql = "UPDATE user SET name = ?, birth_date = ?, password = ?, update_date = NOW() WHERE id = ?";

			// ステートメント生成
			pStmt = conn.prepareStatement(sql);

			// SQLの?パラメータに値を設定
			pStmt.setString(1, name);
			pStmt.setDate(2, sqlBirthDate);
			pStmt.setString(3, cryptWithMD5(password));
			pStmt.setString(4, targetId);

			// 登録SQL実行
			System.out.println(sql);
			pStmt.executeUpdate();

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 ** ユーザーを削除
	 **/
	public void UserDelete(String targetId) {
		Connection conn = null;
		PreparedStatement pStmt = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 実行SQL文字列定義
			String sql = "DELETE FROM user WHERE id = ?";

			// ステートメント生成
			pStmt = conn.prepareStatement(sql);

			// SQLの?パラメータに値を設定
			pStmt.setString(1, targetId);

			// 登録SQL実行
			System.out.println(sql);
			pStmt.executeUpdate();

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	* ログインIDの重複がないかチェック
	*/
	public boolean IdCheck(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 重複なし
			if (!rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		//重複あり
		return false;
	}

	/**
	 ** パスワードを暗号化する
	 */
	public String cryptWithMD5(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;

		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		return result;

	}
}
