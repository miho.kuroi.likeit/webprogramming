<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>delete</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<!--ヘッダー-->
	<nav class="navbar navbar-expand-sm navbar-dark bg-secondary mt-3 mb-3">
		<div
			class="collapse navbar-collapse font-weight-bold justify-content-end">
			<ul class="navbar-nav">
				<li class="nav-item"><a
					class="nav-link disabled text-white mx-3">${userInfo.name}さん</a></li>
				<li class="nav-item"><a
					class="nav-link text-danger border-bottom border-danger mx-5"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</div>
	</nav>

	<!--本体ー-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>ユーザー削除画面</h1>
		</div>

		<form action="UserDeleteServlet" method="post">

			<input type="hidden" name="id" value="${user.id}">

			<div class="question row offset-sm-4">
				<p>
					ログインID: <span>${user.loginId}</span><br> を本当に削除してよろしいでしょうか。
				</p>
			</div>

			<div class="form-group row" align="center" style="padding: 50px 50px">

				<div class="col-sm-5" align="right">
					<a type="button" class="btn btn-outline-secondary btn-lg" href="UserListServlet">キャンセル</a>
				</div>

				<div class="col-sm-5 offset-sm-2" align="left">
					<button type="submit" class="btn btn-outline-secondary btn-lg">&emsp;&emsp;OK&emsp;&emsp;</button>
				</div>

			</div>
		</form>
	</div>
</body>
</html>