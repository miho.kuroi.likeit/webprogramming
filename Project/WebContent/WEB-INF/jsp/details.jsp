<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>




<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>details</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<!--ヘッダー-->
	<nav class="navbar navbar-expand-sm navbar-dark bg-secondary mt-3 mb-3">
		<div
			class="collapse navbar-collapse font-weight-bold justify-content-end">
			<ul class="navbar-nav">
				<li class="nav-item"><a
					class="nav-link disabled text-white mx-3">${userInfo.name}さん</a></li>
				<li class="nav-item"><a
					class="nav-link text-danger border-bottom border-danger mx-5"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</div>
	</nav>

	<!--本体-->
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>ユーザー情報詳細参照</h1>
		</div>


		<!--<form action="UserDetailServlet" method="post"> -->

		<div class="form-group row">
			<label for="staticId" class="col-sm-6 col-form-label" align="right">ログインID</label>
			<div class="col-sm-6" align="left">
				<a type="text" class="form-control-plaintext" id="staticId">${user.loginId}</a>
			</div>
		</div>

		<div class="form-group row">
			<label for="staticUserName" class="col-sm-6 col-form-label"
				align="right">ユーザー名</label>
			<div class="col-sm-6" align="left">
				<a type="text" class="form-control-plaintext" id="staticUserName">${user.name}</a>
			</div>
		</div>

		<div class="form-group row">
			<label for="staticBirthday" class="col-sm-6 col-form-label"
				align="right">生年月日</label>
			<div class="col-sm-6" align="left">
				<a type="date" class="form-control-plaintext" id="staticBirthday">${user.fmtBirthDate()}</a>
			</div>
		</div>

		<div class="form-group row">
			<label for="staticCreated" class="col-sm-6 col-form-label"
				align="right">登録日時</label>
			<div class="col-sm-6" align="left">
				<a type="date" class="form-control-plaintext" id="staticCreated">${user.createDate}</a>
			</div>
		</div>

		<div class="form-group row">
			<label for="staticUpdated" class="col-sm-6 col-form-label"
				align="right">更新日時</label>
			<div class="col-sm-6" align="left">
				<a type="date" class="form-control-plaintext" id="staticUpdated">${user.updateDate}</a>
			</div>
		</div>

		<div class="form-group row" align="right" style="padding-bottom: 20px">
			<div class="col-sm-2">
				<a type="submit" class="text-primary border-bottom border-primary"
					href="UserListServlet">戻る</a>
			</div>
		</div>

		<!-- </form> -->

	</div>
</body>
</html>