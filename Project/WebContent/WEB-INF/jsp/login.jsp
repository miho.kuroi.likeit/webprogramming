<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<div class="container">

		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>ログイン画面</h1>
		</div>


		<form class="form-signin" action="LoginServlet" method="post">

			<!-- ログイン失敗 -->
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger text-center" role="alert">
					${errMsg}</div>
			</c:if>

			<div class="form-group row">
				<label for="inputLoginId" class="col-sm-5 col-form-label"
					align="right">ログインID</label>
				<div class="col-sm-5" align="left">
					<input type="text" name="loginId" id="inputLoginId"
						class="form-control" style="width: 250px;">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputPassword" class="col-sm-5 col-form-label"
					align="right">パスワード</label>
				<div class="col-sm-5" align="left">
					<input type="password" name="password" id="inputPassword"
						class="form-control" style="width: 250px;">
				</div>
			</div>

			<div class="form-group row" align="center" style="padding: 50px 50px">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-outline-secondary">&emsp;ログイン&emsp;</button>
				</div>
			</div>

		</form>
	</div>
</body>
</html>