<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>list</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<!--ヘッダー-->
	<nav class="navbar navbar-expand-sm navbar-dark bg-secondary mt-3 mb-3">
		<div
			class="collapse navbar-collapse font-weight-bold justify-content-end">
			<ul class="navbar-nav">
				<li class="nav-item"><a
					class="nav-link disabled text-white mx-3">${userInfo.name}さん</a></li>
				<li class="nav-item"><a
					class="nav-link text-danger border-bottom border-danger mx-5"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</div>
	</nav>

	<!--本体-->
	<div class="container">



		<div class="col-sm-12 text-center" style="padding: 50px 50px">
			<h1>ユーザー一覧</h1>
		</div>


		<form action="UserListServlet" method="post">

			<input type="hidden" value="${user.id}">

			<div class="form-group row" align="right"
				style="padding-bottom: 20px">
				<div class="col-sm-10">
					<a type="submit" class="text-primary border-bottom border-primary"
						href="UserCreateServlet">新規登録</a>
				</div>
			</div>

			<div class="form-group row">
				<label for="inputId" class="col-sm-4 col-form-label" align="right">ログインID</label>
				<div class="col-sm-6" align="left">
					<input type="id" style="width: 400px;" class="form-control"
						id="inputId" name="loginId">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputPassword" class="col-sm-4 col-form-label"
					align="right">ユーザー名</label>
				<div class="col-sm-6" align="left">
					<input type="text" style="width: 400px;" class="form-control"
						id="inputPassword" name="name">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputBirthday" class="col-sm-4 col-form-label"
					align="right">生年月日</label>
				<div class="col-sm-2" align="left">
					<input type="date" style="width: 170px;" class="form-control"
						name="birthDateFrom">
				</div>
				<span class="form-plaintext">&emsp;～</span>
				<div class="col-sm-2" align="right">
					<input type="date" style="width: 170px;" class="form-control"
						name="birthDateTo">
				</div>
			</div>

			<div class="form-group row" align="right" style="padding: 50px 50px">
				<div class="col-sm-10">
					<button type="submit" class="btn btn-outline-secondary">&emsp;&emsp;検索&emsp;&emsp;</button>
				</div>
			</div>


			<hr class="border-bottom border-secondary col-sm-8">

			<div class="py-5">
				<table class="table table-bordered table-sm col-sm-8 offset-md-2">

					<thead>
						<tr class="d-flex">
							<th class="text-center col-sm-3">ログインID</th>
							<th class="text-center col-sm-3">ユーザー名</th>
							<th class="text-center col-sm-3">生年月日</th>
							<th class="col-sm-3"></th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="user" items="${userList}">
							<tr class="d-flex">
								<td class="col-sm-3">${user.loginId}</td>
								<td class="col-sm-3">${user.name}</td>
								<td class="col-sm-3">${user.fmtBirthDate()}</td>
								<td class="col-sm-3" align="center"><a type="submit"
									class="btn btn-primary btn-sm"
									href="UserDetailServlet?id=${user.id}">詳細</a> <c:if
										test="${userInfo.loginId == 'admin' or userInfo.loginId == user.loginId}">
										<a type="submit" class="btn btn-success btn-sm"
											href="UserUpdateServlet?id=${user.id}">更新</a>
									</c:if> <c:if test="${userInfo.loginId == 'admin'}">
										<a type="submit" class="btn btn-danger btn-sm"
											href="UserDeleteServlet?id=${user.id}">削除</a>
									</c:if></td>
							</tr>
						</c:forEach>
					</tbody>

				</table>
			</div>


		</form>


	</div>


</body>


</html>